=====================
Contribuer au SL3
=====================


:date: 2013-01-20 11:30

Il est possible de contribuer au SL3 de plusieurs manières. Que vous soyez membre du laboratoire ou bien simple étudiant et visiteur du laboratoire, il vous est possible d'y apporter votre contribution.

Pour celà, nous avons rédigé un guide du contributeur. Ce document vous permettra de comprendre comment est organisé le SL3, et comment apporter votre contribution à une des entités constituant le laboratoire.

Télécharger le `guide du contributeur <http://lab-sl3.org/guide-contribution.pdf>`_
