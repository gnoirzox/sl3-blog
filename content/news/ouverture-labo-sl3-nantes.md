Title: Ouverture du laboratoire à Nantes
Date: 2013-01-17 13:56:14
Tags: sl3, nantes

Bonjour,

Le Labo SL3 débarque à Nantes. Ce laboratoire s'intéresse principalement au développement "bas niveau" tel que le fonctionnement des systèmes d'exploitation, l'architecture des ordinateurs, la compilation, la création de langage ou de machine virtuelle telle que la JVM. Ce laboratoire est également là pour aborder d'autres thèmes tel que l'algorithmique, les différents paradigmes de programmation (impératif, objet, fonctionnel, logique etc.) la programmation
concurrente/parallèle avec des langages tels que le C, C++ ou encore le langage Go. 

La programmation est un élément central du laboratoire. Ainsi, un projet au niveau global est prévu. Son but est d'implémenter une machine virtuelle (exemple: la JVM). Ce projet se veut être un vrai projet de recherche. Il est également possible de proposer d'autres projets au niveau local ou global, d'autres projets sont déjà proposés tel qu' un projet d'OS ou encore une alternative à emu8086.

Le rôle de laboratoire au niveau local est de gérer les projets locaux ainsi que organiser des lightnings talks sur différents sujets (machine learning, C++11, etc.). Comme vous le voyez, les activités proposées par ce laboratoire sont diverses et il est ouvert a toute personne s'intéressant et désirant se perfectionner dans ces domaines quel soit son niveau. 

Si vous êtes intéressés par les activités de notre laboratoire, n'hésitez pas à me contacter à [simon.rouger@supinfo.com](mailto:simon.rouger@supinfo.com) !
