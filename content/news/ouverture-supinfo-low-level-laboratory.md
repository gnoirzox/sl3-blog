Title: Ouverture du SUPINFO Low-Level Laboratory (SL3)
Date: 2013-01-10 18:00:00
Tags: sl3, montreal

Bonjour,

c'est avec joie que l'équipe du SL3 est fière de vous annoncer la création de ce laboratoire. L'objectif de ce projet est de ressembler les étudiants autour de tout ce qui se rapproche du &laquo; bas-niveau &raquo; c'est-à-dire des couches informatiques les plus basses. Au delà d'un simple domaine, la philosophie est d'explorer en profondeur les outils qu'un développeur utilise chaque jour.

<img src="http://i.imgur.com/tKJ0W.png" style="float: left; padding: 5px;" />

Concrètement, le SL3 abordera des sujets comme tout ce qui gravite autour de l'étude des systèmes d'exploitations, langages informatiques et du développement. Comment fonctionne un driver sous Linux ? Comment créer un système d'exploitation ? Que se passe-t'il quand on compile un fichier source ? Voici le genre de problématique auxquelles nous nous intéressons.

Pour en savoir plus les intentions du SL3, je vous invite à lire la page de présentation : [Présentation](/pages/presentation-du-sl3.html).

Si vous avez des questions ou que vous souhaitez vous inscrire, n'hésitez pas à envoyer un mail à [timothee.bernard@supinfo.com](mailto:timothee.bernard@supinfo.com).

N'hésitez pas non plus à venir nous rejoindre sur [nos forums](/forums/) ou sur notre channel IRC : #sl3@irc.supnetwork.org !
