Title: Gestion de la mémoire sur l'architecture x86
Author: David Delassus
Date: 2013-03-09 12:00:00
Tags: os, x86

Au travers de cet article, nous verrons comment le système d'exploitation gère la
mémoire sur les processeurs de type x86. Nous verrons donc comment fonctionne la
mémoire segmentée, la mémoire paginée, comment fonctionne l'allocation de mémoire
et nous ferons une petite introduction à la mémoire partagée.

_NB: Cet article contient des informations de l'article [Paging](http://wiki.osdev.org/Paging)
sur [OSDev](http://wiki.osdev.org)._

# Mémoire segmentée

Lorsque les systèmes d'exploitation fonctionnaient encore en 16-bits, la mémoire
était organisée en segment, chacun de ces segments étaient définis dans les registres
du micro-processeur :

* **CS** : l'emplacement du code ;
* **DS** : l'emplacement des données ;
* **SS** : l'emplacement de la pile ;
* **ES**, **FS**, **GS** : des segments supplémentaires.

Chaque programme a ainsi accès à la totalité de la mémoire physique. Ce système
est peu pratique, car si un programme vient à corrompre la mémoire, c'est tout
le système qui est compromis.

# Mémoire paginée

C'est dans ce contexte, avec l'arrivée du 32-bits, qu'est apparu le concept de
mémoire paginée. Le fonctionnement est simple : la mémoire n'est plus divisée en
segment, mais en page.

On définit donc le principe de mémoire virtuelle. Pour faire simple, chaque tâche
**croit** pouvoir accéder à la totalité de la mémoire **physique**, chaque adresse
virtuelle est _mappée_ sur une adresse _physique_. Ainsi si le programme A veut
accéder à l'adresse 0x1000, il accédera en réalité à l'adresse physique 0x2000,
et si un programme B veut accéder à l'adresse 0x1000, il accédera en réalité à
l'adresse physique 0x3000.

Ainsi, le processeur a connaissance de ce _mappage_ au travers de tables qui associent
une adresse virtuelle à une adresse physique.

<center>![schéma de mémoire paginée](http://wiki.osdev.org/images/a/a7/Virtual_memory.png)</center>

Le processeur est capable de traduire une adresse virtuelle en adresse physique
grâce au **MMU** (**M**emory **M**anagment **U**nit).

## Mettre en place la mémoire paginée

Pour faire la traduction adresse virtuelle <-> adresse physique, le _MMU_ a besoin
de certaines structures :

* un répertoire de page (_Page Directory_) : il s'agit d'un tableau contenant
l'adresse physique de tables de pages, il peut contenir 1024 entrées de chacune
4 octets et son adresse physique est stockée dans le registre **CR3** afin que le
_MMU_ puisse faire la traduction.
* des tables de pages (_Page Table_) : ce sont des tableaux de 1024 entrées (de
4 octets), où chaque entrée contient l'adresse physique d'une page.

### Structure d'une entrée du répertoire de page

Une entrée a une taille de 4 octets (32 bits) :

<center>![entrée du répertoire de page](http://wiki.osdev.org/images/9/94/Page_dir.png)</center>

* S : définit la taille d'une page (0 pour 4ko, 1 pour 4Mo) ;
* A : définit s'il y a eu un accès en lecture/écriture à la page ;
* D : définit si la page peut-être mise en cache ;
* W : définit le mode de mise en cache (_Write-Through_ si 1, _Write-Back_ sinon) ;
* U : définit si la page peut-être accédée par tout le monde ou non ;
* R : définit si la page peut-être accédée en écriture ;
* P : définit si la page est présente dans la mémoire physique, si la page a été
placée dans la _swap_ (sur le disque dur), alors elle n'est pas présente et un
accès à cette page provoquera un _Page Fault_ que le système doit gérer (récupérer
la page de la _swap_ pour la remettre en RAM).

### Structure d'une entrée de la table de page

De même que pour le répertoire de page, une entrée a une taille de 4 octets :

<center>![entrée d'une table de page](http://wiki.osdev.org/images/9/9b/Page_table.png)</center>

* G : s'il est défini, cela empêche le **TLB** (**T**ranslation **L**ookaside **B**uffer
de mettre à jour l'adresse dans son cache si le registre **CR3** est redéfini,
le registre **CR4** doit être défini correctement pour activer cette fonctionnalité) ;
* D : défini s'il y a eu un accès en écriture à la page ;
* C : équivalent du bit D du répertoire de page ;
* le reste est équivalent au répertoire de page.

### Récapitulatif

<center>![schéma de la structure de la pagination sur le x86](http://ompldr.org/vaHBhOQ/Paging_Structure.gif)</center>

_Le cadre rouge représente une adresse virtuelle._

#### INVLPG

``INVLPG`` est une instruction, disponible depuis le _i468_, elle permet d'invalider
une entrée dans le _TLB_.

### Détails sur le TLB

Le _TLB_ est un cache de traduction de page mémoire, lorsque le processeur veut
traduire une adresse virtuelle en adresse physique, le _TLB_ est consulté. Si
l'adresse est présente dans le _TLB_, aucune traduction n'est faite, sinon le
_MMU_ réalise la traduction et place le résultat dans le _TLB_.

### Fonctionnement de la traduction adresse virtuelle <-> adresse physique

_Les informations de cette section sont tirées de l'article [suivant](http://a.michelizza.free.fr/pmwiki.php?n=TutoOS.Mm)_

_NB: Une adresse virtuelle est également appelée adresse linéaire_

1. le processeur utilise le registre **CR3** pour connaître l'adresse physique du
répertoire de pages ;
2. les 10 premiers bits de l'adresse virtuelle forment un offset (entre 0 et 1023),
qui identifie une entrée du répertoire de page ;
3. cette entrée contient l'adresse physique d'une table de pages ;
4. les 10 bits suivants de l'adresse virtuelle forment un offset (toujours entre
0 et 1023) qui identifie une entrée dans la table de pages ;
5. l'entrée de la table de page identifie une page de 4ko dans la mémoire physique ;
6. les 12 derniers bits de l'adresse virtuelle forment un offset (compris en 0 et
4095) qui permet d'identifier l'adresse physique.

<center>![schéma de la traduction d'adresse](http://a.michelizza.free.fr/uploads/TutoOS/paging_memory.png)</center>

### Activer la pagination

Pour activer la pagination, il suffit de placer l'adresse physique du répertoire
de page dans le registre **CR3** et d'activer le bit de pagination du registre
**CR0** :

    :::asm
    mov eax, [page_directory]
    mov cr3, eax

    mov eax, cr0
    or eax, 0x80000000
    mov cr0, eax

## Espaces d'adressages

Comme vu ci-dessus, on a donc deux types d'adresses :

* les adresses virtuelles (ou linéaires) ;
* les adresses physiques.

Une fois la pagination mise en place, toutes les adresses (pour les pointeurs, etc...)
sont des adresses virtuelles (il est donc important de _mapper_ l'espace d'adressage
du système d'exploitation avant d'activer la pagination).

Avec ce principe d'adresse virtuelle, chaque processus croit qu'il peut accéder
à la totalité de la mémoire, et même plus.

**Q : Mais comment un processus peut accéder à plus de mémoire que disponible ?**

R : Si on mappe dans l'espace d'adressage virtuelle des adresses présentes sur
un autre périphérique de stockage (disons le disque dur par exemple), on a ainsi
accès à plus de mémoire.

**Q: C'est comme ça que fonctionne la _swap_ ?**

R : Non, lorsqu'il n'y a plus de page disponible pour l'allocation, le système
d'exploitation va placer certaines pages dont le A (rappel: s'il y a eu un accès
en lecture/écriture sur la page) est à 0, sur le disque dur, et lorsqu'un accès
à ces pages sera demandé (le bit P, présent en mémoire, vaut 0), alors le système
les replacera en RAM.

## Changement de contexte et parallélisme

Les systèmes d'aujourd'hui sont multitâches. Et dans un système qui utilise la
pagination, chaque processus possède son propre espace d'adressage virtuel. Cela
implique que chaque processus à son propre répertoire de pages. Quand l'ordonnanceur
du système va passer d'un processus à l'autre, on changera la valeur du registre
**CR3** afin qu'il pointe vers le bon répertoire de pages (celui du processus que
l'on veut exécuter), on appelle cela le **changement de contexte**.

Concernant le parallélisme, nous avions parlé dans l'article concernant [fork](http://www.lab-sl3.org/articles/news/le-parallelisme-avec-fork.html)
de ce qu'il se passait réellement lors d'un ``fork``. On peut maintenant décrire
en détail ce qu'il va se passer (je ne présenterai pas le principe de **copy-on-write**) :

1. clonage du répertoire de pages dans le nouveau processus :
    1. création d'un nouveau répertoire de pages ;
    2. clonage des tables de pages dans le nouveau répertoire :
        1. désactivation de la pagination ;
        2. copie de la mémoire physique du premier processus vers le nouveau ;
        3. réactivation de la pagination.
2. création du nouveau processus ;
3. attachement du nouveau répertoire de pages à ce processus ;
4. ajout du processus à la liste des processus de l'ordonnanceur.

Pour la mise en place du **copy-on-write**, l'algorithme est plus complexe, là on
copie simplement la totalité de la mémoire utilisée par le processus.

## Au sein de l'espace d'adressage virtuel

Chaque processus organise son espace d'adressage de manière segmentée, ainsi lors
du _changement de contexte_, la valeur des registres du micro-processeur est mise
à jour, c'est le rôle du système d'exploitation de garder une trace de ces valeurs
afin de réaliser cette mise à jour.

# Allocation de mémoire

Maintenant que l'on sait exactement comment fonctionne la gestion de la mémoire,
nous allons nous attaquer à ce qui nous intéresse : comment fonctionne l'allocation ?

On a déjà parlé dans un [article précédent](http://www.lab-sl3.org/articles/news/fonctionnement-et-implementation-dun-gestionnaire-dallocation-en-c.html)
du fonctionnement de l'allocation dynamique au sein d'un programme. On avait
utilisé l'appel système ``sbrk``.

## Fonctionnement des appels systèmes brk et sbrk

_NB: Les schémas présents dans cette section sont tirés du PDF [Malloc Tutorial](http://www.inf.udec.cl/~leo/Malloc_tutorial.pdf)._

Chaque processus possède un tas, avec trois bornes :

* son début ;
* sa taille maximum ;
* le **break**, qui délimite l'espace alloué et l'espace non-alloué.

<center>![schéma du tas](http://ompldr.org/vaG95Nw/heap-schema.png)</center>

Tenter d'accéder à une adresse au delà du _break_ provoquera une erreur de _bus_.
Les appels systèmes ``brk`` et ``sbrk`` permettent de modifier ce _break_ :

* ``int brk (void *addr)`` : défini l'adresse du _break_, il retourne ``0`` en
cas de succès, et ``-1`` en cas d'erreur (et ``errno`` est défini à ``ENOMEM``) ;
* ``void *sbrk (intptr_t increment)`` : incrémente l'adresse du _break_, il retourne
la précédente adresse du _break_ en cas de succès, ou ``(void *) -1`` en cas d'erreur
(et ``errno`` vaut ``ENOMEM``).

Comme vous le savez, la mémoire est organisée en page, le tas n'y échappe pas :

<center>![schéma du tas paginé](http://ompldr.org/vaG95YQ/heap-schema2.png)</center>

On peut voir sur ce schéma que le _break_ est en plein milieu d'une page, cette
zone mémoire entre le _break_ et la fin de la page est appelée _no-man's land_.

Cette mémoire est accessible et un accès au delà de la limite du _break_ peut
fonctionner pour de petite portion de mémoire, mais provoquera une erreur si l'on
dépasse la limite de la page. Il peut donc s'avérer dangereux d'écrire dans cette
zone, bien qu'accessible.

Lorsque ``brk`` et ``sbrk`` vont redéfinir le _break_, le système déterminera si
oui ou non il faut allouer une nouvelle page mémoire.

## Fonctionnement de l'appel système mmap

Un appel de ``mmap`` demande au noyau de _mapper_ un certain nombre d'octet d'un
objet représenté par un descripteur de fichier à une certaine adresse si on le
spécifie :

    :::c
    #include <sys/mman.h>

    void *mmap (void *addr, size_t len, int prot, int flags, int fd, off_t offset);

* ``addr`` : spécifie l'adresse où l'on souhaite que le fichier soit _mappé_ (ou
``NULL`` pour laisser le noyau décider) ;
* ``len`` : taille des données à _mapper_ ;
* ``prot`` : défini les paramètres d'accès aux pages nouvellement _mappée_ :
    * ``PROT_NONE`` : on ne peut pas accéder aux pages (inutile) ;
    * ``PROT_READ`` : les pages peuvent être accédées en lecture ;
    * ``PROT_WRITE`` : les pages peuvent être accédées en écriture ;
    * ``PROT_EXEC`` : les pages peuvent être exécutée.
* ``flags`` : défini le type du _mapping_ :
    * ``MAP_FIXED`` : Oblige ``mmap`` à placer le _mappage_ à l'adresse exacte demandée
(``addr`` doit être un multiple de la taille d'une page), si la région spécifiée par
``addr`` et ``len`` déborde sur des pages déjà _mappées_, alors ces pages sont _démappées_
pour laisser la place aux nouvelles, son usage est déconseillé ;
    * ``MAP_PRIVATE`` : les pages nouvellement _mappées_ ne sont pas partagées entre
les processus, s'il y a un ``fork``, alors le principe du **copy-on-write** s'applique
et les modifications faites ne sont pas reflétées dans le _mapping_ des autres processus ;
    * ``MAP_SHARED`` : les pages nouvellement _mappées_ sont partagées avec tous les
autres processus qui _mappent_ le même fichier, écrire dans ce _mapping_ équivaut
à écrire dans le fichier, et lire dedans reflétera les écritures des autres processus ;
    * ``MAP_ANONYMOUS`` : le _mappage_ n'est pas associé à un fichier (il s'agit
donc d'une allocation de mémoire), ainsi ``fd`` doit être à ``-1`` ;
    * pour les autres options, je vous laisse voir le manuel.
* ``fd`` : le descripteur de fichier à _mapper_ ;
* ``offset`` : offset au sein du fichier à partir duquel on va _mapper_ dans l'espace
d'adressage virtuel.

``mmap`` retourne le pointeur vers la mémoire nouvellement _mappée_ en cas de succès,
ou ``MAP_FAILED`` (soit ``(void *) -1``) en cas d'erreur (et ``errno`` est défini).

<center>![schéma de mmap](http://images.devshed.com/ds/stories/Advanced_File_IO/image_1.JPG)</center>

### Lire un fichier avec mmap

    :::c
    #include <stdio.h>
    #include <stdlib.h>

    #include <unistd.h>
    #include <fcntl.h>

    #include <sys/stat.h>
    #include <sys/types.h>
    #include <sys/mman.h>

    struct file_t
    {
        int fd;
        char *content;
        size_t size;
    };

    struct file_t *read_file (char const * const filename)
    {
        struct file_t *f = malloc (sizeof (struct file_t)); /* don't check for simplicity */
        struct stat buf;

        f->fd = open (filename, O_RDONLY);

        if (f->fd < 0)
        {
            fprintf (stderr, "Can't read %s\n", filename);
            free (f);
            return NULL;
        }

        if (fstat (f->fd, &buf) < 0)
        {
            fprintf (stderr, "Can't get file size\n");
            close (f->fd);
            free (f);
            return NULL;
        }

        f->size = buf.st_size;

        /* we map the whole file, privately */
        f->content = mmap (NULL, f->size, PROT_READ, MAP_FILE | MAP_PRIVATE, f->fd, 0);

        if (f->content == MAP_FAILED)
        {
            fprintf (stderr, "Can't map file\n");
            close (f->fd);
            free (f);
            return NULL;
        }

        return f;
    }

    void close_file (struct file_t *f)
    {
        /* unmap memory */
        munmap (f->content, f->size);
        close (f->fd);
        free (f);
    }

### Autres cas d'utilisations

Dans certaines implémentations, ``malloc()`` utilise ``mmap`` pour allouer de la
mémoire (notamment quand la taille demandée dépasse la taille d'une page).

On peut également mettre en place un système de mémoire partagée (anonyme ou nommée)
avec, nous verrons cela après.

# Fonctionnement de la mémoire partagée

Dans certains cas, deux processus ont besoin d'échanger des données, la mémoire
partagée permet cela.

Le principe est simple, dans l'espace d'adressage virtuel des deux programmes,
chacun aura une ou plusieurs pages qui _mappent_ le **même** espace physique (ou
**frame**). Ainsi, ils peuvent lire et écrire dans cette mémoire pour échanger
des données.

<center>![schéma de la mémoire partagée](http://ompldr.org/vaHAwZQ/sharedmem.png)</center>

## Une mémoire partagée avec mmap

    :::c
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>

    #include <unistd.h>
    #include <fcntl.h>

    #include <sys/mman.h>

    int main (void)
    {
        char *sharedmem = NULL;
        int child_pid   = 0;

        /* allocation of shared memory */
        sharedmem = mmap (NULL, 4096, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);

        if (sharedmem == MAP_FAILED)
        {
            fprintf (stderr, "Can't allocate shared memory\n");
            exit (EXIT_FAILURE);
        }

        strcpy (sharedmem, "string 1");

        printf ("Parent: %s\n", sharedmem);

        /* now fork ! */
        child_pid = fork ();

        switch (child_pid)
        {
            case -1:
                fprintf (stderr, "Can't fork\n");
                munmap (sharedmem, 4096);
                exit (EXIT_FAILURE);

            /* child process */
            case 0:
                printf ("Child: %s\n", sharedmem);

                sleep (3);

                printf ("Child: %s\n", sharedmem);

                munmap (sharedmem, 4096);
                exit (EXIT_SUCCESS);

            /* parent process */
            default:
                sleep (2);

                strcpy (sharedmem, "string 2");

                printf ("Parent: %s\n", sharedmem);

                /* wait for the child to finish */
                sleep (2);

                munmap (sharedmem, 4096);
                exit (EXIT_SUCCESS);
        }

        return 0;
    }

Dans ce programme :

* on alloue un espace mémoire partagé ;
* on y écrit une chaîne de caractère que l'on affiche dans le parent ;
* dans l'enfant on affiche le contenu de la mémoire partagée ;
* on attend 3 secondes dans l'enfant, puis on affiche à nouveau la mémoire partagé, et on quitte ;
* pendant ce temps la, dans le parent, on attend 2 secondes, puis on redéfinit le
contenu de la mémoire partagée, et on l'affiche, on attend encore 2 secondes et on quitte.

Voici le résultat :

    :::console
    Parent: string 1
    Child: string 1
    Parent: string 2
    Child: string 2

Comme on peut le voir, l'échange de données a eu lieu.

Cependant, il faut faire attention à ne pas lire en même temps qu'un autre processus
écrit, mettre en place un système de _lock_ permettrait d'éviter ce genre de
désagrément et on en arrive à faire de la **concurrence**.

### Mémoire partagée avec les Unices

Les systèmes de type UNIX fournissent une API permettant de mettre en place une mémoire partagée entre
deux processus distinct :

    :::c
    int shmget (key_t key, size_t size, int shmflg);

* ``key`` : Clé identifiant le segment de mémoire partagée ;
* ``size`` : Taille du segment de mémoire partagée ;
* ``shmflg`` : Permissions d'accès au segment de mémoire partagée et ces quelques flags :
    * ``IPC_CREAT`` : Créé le nouveau segment ;
    * ``IPC_EXCL`` : Utilisé avec ``IPC_CREAT`` pour prévenir des erreurs si le segment existe déjà ;
    * ``SHM_HUGETLB`` : Alloue le segment dans les _grandes pages_ (de plus de 4ko de taille) ;
    * ``SHM_NORESERVE`` : N'alloue pas d'espace dans la _swap_ pour cette page,
ainsi si la page est en _swap_, un accès à celle-ci provoquera une erreur de segmentation
au lieu d'un _Page Fault_ (qui permet en temps normal de replacer la page dans la
mémoire physique).
* Elle retourne ensuite l'identifiant du segment de mémoire partagée.

Pour changer les permissions et autres caractéristiques du segment de mémoire partagée :

    :::c
    int shmctl (int shmid, int cmd, struct shmid_ds *buf);

* ``shmid`` : Identifiant du segment de mémoire partagée ;
* ``cmd`` : Commande à exécuter sur le segment de mémoire partagée :
    * ``SHM_LOCK`` : Pose un _lock_ sur le segment, le processus doit être en root ;
    * ``SHM_UNLOCK`` : Enlève un _lock_ sur le segment, le processus doit être en root ;
    * ``IPC_STAT`` : Retourne les informations du segment et les place dans le buffer
``buf``, le processus doit avoir accès en lecture au segment ;
    * ``IPC_SET`` : Définit l'identification utilisateur/groupe et les permissions
d'accès au segment, le processus doit être le propriétaire ou le créateur du segment,
ou être root ;
    * ``IPC_RMID`` : Supprime le segment de mémoire partagée.
* ``buf`` : Buffer pour les résultats.

Pour attacher le segment de mémoire partagée à un pointeur et pour le détacher :

    :::c
    void *shmat (int shmid, const void *shmaddr, int shmflg);
    int shmdt (const void *shmaddr);

* ``shmat`` : Retourne un pointeur vers le segment identifié par ``shmid`` ;
* ``shmdt`` : Détache le segment pointé par ``shmaddr``.

Ces deux dernières fonctions ont pour effet de _mapper_ et _démapper_ le segment
dans l'espace d'adressage virtuel.

**Exemple d'utilisation :**

Voici un exemple de comment créer un segment de mémoire partagée :

    :::c
    #include <sys/types.h>
    #include <sys/ipc.h>
    #include <sys/shm.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>

    #define SHM_SIZE    27

    int main (void)
    {
        struct shmid_ds shmid_ds;

        key_t key = 5678;
        int shmid = -1;
        char *shm = NULL;
        char *s   = NULL;
        char c    = 0;

        /* create the shared memory segment */
        shmid = shmget (key, SHM_SIZE, IPC_CREAT | 0666);

        if (shmid < 0)
        {
            perror ("shmget");
            exit (EXIT_FAILURE);
        }

        /* attach the shared memory segment in our address space */
        shm = shmat (shmid, NULL, 0);

        if (shm == (char *) -1)
        {
            perror ("shmat");
            exit (EXIT_FAILURE);
        }

        /* write data in our segment */
        s = shm;

        for (c = 'a' ; c <= 'z' ; ++c)
        {
            *s = c;
            s++;
        }

        *s = 0;

        /* now, we wait until the other process changes the first
         * character of our memory to '*', indicating that it has
         * read what we put there.
         */

        while (*shm != '*')
        {
            sleep (1);
        }

        /* detach the segment */
        shmdt (shm);

        /* remove it */
        if (shmctl (shmid, IPC_RMID, &shmid_ds) < 0)
        {
            perror ("shmctl");
            exit (EXIT_FAILURE);
        }


        return EXIT_SUCCESS;
    }

Et comment récupérer un segment de mémoire partagée dans un autre processus :

    :::c
    #include <sys/types.h>
    #include <sys/ipc.h>
    #include <sys/shm.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>

    #define SHM_SIZE    27

    int main (void)
    {
        key_t key = 5678;
        int shmid = -1;
        char *shm = NULL;
        char *s   = NULL;

        /* get the shared memory segment */
        shmid = shmget (key, SHM_SIZE, 0666);

        if (shmid < 0)
        {
            perror ("shmget");
            exit (EXIT_FAILURE);
        }

        /* attach the shared memory segment in our address space */
        shm = shmat (shmid, NULL, 0);

        if (shm == (char *) -1)
        {
            perror ("shmat");
            exit (EXIT_FAILURE);
        }

        /* read data */
        for (s = shm ; *s != 0; ++s)
        {
            putchar (*s);
        }

        putchar ('\n');

        /* we readthe data, change the first byte */
        *shm = '*';

        /* dettach segment */
        shmdt (shm);

        return EXIT_SUCCESS;
    }

Si tout se passe bien, le second processus va afficher :

    :::console
    abcdefghijklmnopqrstuvwxyz

Comme vous pouvez le voir, on utilise le premier caractère comme indicateur pour
prévenir le premier processus que les données ont été lues.

On peut imaginer placer une structure dans cette mémoire avec dedans un entier
déterminant l'état de la mémoire :

* prêt pour la lecture ;
* en cours de lecture ;
* prêt pour l'écriture ;
* en cours d'écriture.

Mettre en place un tel système permet de faire un simili de concurrence entre deux
processus.

# Conclusion

C'est la fin de cet article, d'autres articles concernant le fonctionnement du
processeur x86 et d'autres composants du système d'exploitation (GDT, IDT, ...)
viendront s'ajouter à celui ci.
