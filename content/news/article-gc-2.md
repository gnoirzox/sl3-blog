Title: Fonctionnement d'un Garbage Collector (Partie 2)
Author: David Delassus
Date: 2013-03-23 12:00:00

Dans [mon article précédent](http://www.lab-sl3.org/articles/news/fonctionnement-dun-garbage-collector.html),
je vous ai introduit le rôle d'un _Garbage Collector_ : faciliter la gestion
de la mémoire.

Il existe différentes méthodes, et celle que je vous ai présenté est la plus simple
et intuitive : le **reference counting**.

## Le reference counting

Le principe est simple, pour chaque portion de mémoire qu'on alloue, on a un
compteur qui indique le nombre de référence que l'on a sur la mémoire en question,
quand ce compteur atteint 0, plus personne n'utilise la mémoire et on peut donc
la libérer.

Ce principe est utilisé par la **GLib** avec _GObject_ (et donc, par **GTK+**).

Imaginons un cas particulier :

1. j'alloue un objet ``A`` ;
2. à l'initialisation de ``A``, il alloue un objet ``B`` ;
3. à l'initialisation de ``B``, ce dernier référence l'objet ``A`` ;
4. je supprime la référence de ``A`` que j'ai.

Voyons ce qu'il se passe dans ce cas :

1. le compteur de référence de ``A`` vaut 1 ;
2. le compteur de référence de ``A`` vaut 1, celui de ``B`` vaut 1 ;
3. le compteur de référence de ``A`` vaut 2, celui de ``B`` vaut 1 ;
4. le compteur de référence de ``A`` vaut 1, celui de ``B`` vaut 1.

Après l'étape 4, le programme n'a plus aucun accès ni à ``A``, ni à ``B``, on dit
qu'ils sont **unreachable** (inatteignable). Pourtant, leurs compteurs de référence
valent tout deux 1, ils ne sont donc pas libérés, à cause de la référence circulaire.

Le *reference counting* n'empêche donc pas les fuites de mémoire, il faut être
vigilant.

## Les weak reference

Une **weak reference** (référence faible) est une référence vers une zone mémoire
qui n'empêche pas cette mémoire d'être libérée par le _Garbage Collector_ :

    :::c
    int *ptr     = gc_alloc (sizeof (int));  /* ptr est une 'strong reference' */
    int *ref     = gc_ref (ptr);             /* ref est une 'strong reference' */
    int *weakref = ptr;                      /* weakref est une 'weak reference' */

    gc_unref (ref);

    /* l'objet n'est pas libéré, on a toujours une strong reference
     * vers ce dernier
     */

    gc_unref (ptr);

    /* l'objet est libéré, il n'y a plus aucune strong reference vers
     * ce dernier
     */

    *weakref = 5;

    /* provoque un segmentation fault, car bien que weakref référence toujours
     * l'objet en question, weakref n'est pas une strong reference, ce qui n'a
     * pas empêché ce dernier d'être libéré.
     */

## Initialiser la mémoire, constructeur et destructeur

Les *weak reference* peuvent donc être une solution pour l'algorithme de *reference
counting*, en faisant en sorte que notre objet ``B`` maintienne une *weak reference*
sur notre objet ``A``, on évite ainsi la référence circulaire.

Cependant, dans l'état, ce n'est pas suffisant car à la libération de ``A``, ``B``
n'est toujours pas libéré.

L'idéal serait d'avoir un constructeur et un destructeur, qui permettent d'allouer
ou de désallouer la mémoire référencée par un objet (par objet j'entends zone mémoire).

On pourrait imaginer ce genre de cas :

    :::c
    typedef struct mastruct mastruct_t;
    typedef struct mastruct2 mastruct2_t;

    struct mastruct
    {
        mastruct2_t *b;
    };

    struct mastruct2
    {
        mastruct_t *a;
    };

    void mastruct_constructor (void *mem)
    {
        mastruct_t *a = (mastruct_t *) mem;

        a->b = gc_alloc (sizeof (mastruct2_t)); /* par defaut, pas de constructeur / destructeur */

        a->b->a = a; /* weak reference pour eviter les references circulaires */
    }

    void mastruct_destructor (void *mem)
    {
        mastruct_t *a = (mastruct_t *) mem;

        a->b->a = NULL; /* on supprime la reference de B vers A */

        gc_unref (a->b); /* on supprime notre reference sur B */
    }

    int main (void)
    {
        mastruct_t *a = gc_alloc_full (
            sizeof (mastruct_t),
            mastruct_constructor,
            mastruct_destructor
        );

        /* apres allocation, mastruct_constructor est appele,
         * elle alloue B et l initialise
         */

        mastruct2_t *b = gc_ref (a->b);

        /* do stuff with a */

        gc_unref (a);

        /* ici, plus de strong reference sur a, son compteur arrive a 0,
         * mastruct_destructor est appele, la memoire est liberee
         */

        /* do stuff with b */
        /* attention, b->a vaut NULL ! */

        gc_unref (b);

        /* plus de strong reference sur b, la memoire est liberee */
    }

L'inconvénient du *reference counting* est donc que le développeur doit encore
s'assurer de la bonne gestion de la mémoire. Il doit encore beaucoup mettre la
main à la pâte, avec la différentiation des *strong references* et des *weak
references*, quand les utiliser, initialiser correctement la mémoire, etc...

Au final, le *reference counting* nous apporte juste la certitude de ne jamais
accéder à une mémoire non-allouée (et pour peu qu'on utilise pas de *weak references*).
Ce n'est pas suffisant.

## Le mark-and-sweep

Cet algorithme est bien plus performant car il n'implique pas le développeur dans
son déroulement.

Ce type de _Garbage Collector_ définit deux types d'objets (zones mémoire) :

* les objets **racines** : ceux que le développeur déclare, ils sont présent
dans les registres, les variables globales ou sur la pile (variables locales à une
fonction) ;
* les autres objets sont accessibles directement ou indirectement via les objets
racines.

L'important dans cet algorithme est de connaître à tout moment quelles sont les
références détenues par un objet. Ce qui nous permet d'organiser notre liste
d'objets en arbre.

Tous les objets inaccessibles directement ou indirectement via un objet *racine*
sont considérés comme *unreachable* et seront libérés lors de l'exécution de
l'algorithme.

Cet algorithme n'est lancé que lorsqu'un objet est alloué :

    :::text

    function gc_alloc (size)
        if available size < size
        then
            for each root object r
            do
                mark (r)
            end

            sweep ()
        end

        allocate object
        return allocated object
    end

    function mark (Object p)
        if not p.marked
        then
            p.marked = true

            for each object q referenced by p
            do
                mark (q)
            end
        end
    end

    function sweep ()
        for each object p
        do
            if p.marked
                p.marked = false
            else
                free (p)
            end
        end
    end

Comme le montre cet algorithme :

* on parcourt l'arbre des objets accessibles directement ou indirectement ;
* chacun de ces objets est marqué du passage de l'algorithme ;
* une fois le marquage fait, on passe à la phase de nettoyage ;
* chaque objet marqué, on supprime la marque ;
* chaque objet non-marqué est libéré.

Les objets non marqués sont les objets *unreachable*, car le programme n'a plus
aucun moyen d'y accéder.

<center>![schéma du mark-and-sweep](http://www.brpreiss.com/books/opus5/html/img1708.gif)</center>

Dans le schéma ci-dessus :

* l'étape ``a`` correspond à l'état de la mémoire avant le *mark-and-sweep* ;
* l'étape ``b`` correspond à l'état de la mémoire après le *mark* ;
* l'étape ``c`` correspond à l'état de la mémoire après le *sweep*.

Cet algorithme prend place directement dans le *HEAP* du programme, il est donc
ardu de l'implémenter correctement de manière portable.

## Boehm Garbage Collector

Fort heureusement, nous n'aurons pas à l'implémenter. À la place, nous apprendrons
à utiliser le **Boehm-demers-Weiser garbage collector** (du nom des auteurs initiaux
du projet), ou plus simplement **Boehm GC**.

Son usage est extrêmement simple :

* inclure ``gc.h`` ;
* appeler ``GC_INIT()`` au début du programme (avant toute allocation) ;
* remplacer tous les appels de ``malloc()`` et ``calloc()`` par ``GC_MALLOC()`` ;
* supprimer tous les appels de ``free()``.

Exemple provenant de [https://en.wikipedia.org/wiki/Boehm_garbage_collector](https://en.wikipedia.org/wiki/Boehm_garbage_collector) :

    :::c
    #include <assert.h>
    #include <stdio.h>
    #include <gc.h>

    int main (void)
    {
        int i;

        GC_INIT ();

        for (i = 0; i < 10000000; ++i)
        {
            int **p = (int **) GC_MALLOC (sizeof (int *));
            int *q  = (int *) GC_MALLOC_ATOMIC (sizeof (int));

            assert (*p == 0);

            *p = (int *) GC_REALLOC (q, 2 * sizeof (int));

            if (i % 100000 == 0)
            {
                printf ("Heap size = %zu\n", GC_get_heap_size ());
            }
        }

        return 0;
    }


## Conclusion

Le **mark-and-sweep** à l'avantage d'éviter toute fuite de mémoire et offre un
recyclage de celle-ci très efficace, bien qu'il soit assez coûteux en ressources,
car il doit parcourir l'ensemble des objets pour chaque allocation.
