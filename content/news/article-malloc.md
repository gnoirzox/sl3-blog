Title: Fonctionnement et implémentation d'un gestionnaire d'allocation en C  
Author: David Delassus
Date: 2013-02-11 18:00:00
Tags: c, os

Un des points importants en programmation bas niveau est la gestion de la mémoire.
Ainsi bien comprendre le fonctionnement du gestionnaire de mémoire (malloc, free, etc) est nécessaire.

Au travers de cet article, nous explorerons comment le système d'exploitation
gère la mémoire (mémoire segmentée, paginée, ...) et comment écrire notre propre
gestionnaire de mémoire (malloc, calloc, realloc, free).

## Le fonctionnement de l'allocation dynamique

Pour rappel, la structure d'un programme en mémoire est la suivante :

<center>![Schéma de la structure d'un programme](http://ompldr.org/vaGZrNA)</center>

L'allocation dynamique se fait au moyen de l'appel système **sbrk**. Cet appel
système prend en paramètre la taille des données à allouer et renvoie l'adresse de
ces données sur le tas (*heap* en anglais).

Lorsque l'on appelle **sbrk**, ce dernier ajoute la taille des données à allouer
au sommet du tas :

    var HeapSize  := 4096
    var HeapStart := <start address of heap == first address after BSS segment>
    var HeapEnd   := HeapStart + HeapSize

    syscall sbrk (size)
    begin
        var address := HeapEnd

        HeapEnd += size

        return address
    end

C'est cet appel système que la fonction ``malloc()`` utilise.

## Créer son propre gestionnaire de mémoire

Nous allons donc utiliser l'appel système **sbrk** pour allouer de la mémoire.
Le problème suivant se pose, comment allons nous désallouer la mémoire ?

Pour cela, nous allons ajouter une en-tête à chacune de nos allocations mémoire,
précisant la taille du bloc de donnée et s'il est occupé. Pour désallouer un bloc
de données, il suffira de préciser que ce bloc est libre :

    type block is
    begin
        int size
        bool used
    end

### Allouer de la mémoire

Le fonctionnement de ``malloc()`` sera de chercher le premier bloc de données
libre dont la taille est supérieure ou égale à la taille que l'on veut allouer.
On définit ensuite ce bloc comme étant occupé.

    :::c
    /* informations sur le tas */
    char *HeapStart = 0;
    char *HeapEnd   = 0;

    /* notre en-tête */
    struct malloc_header_t
    {
        unsigned int size : 31;
        unsigned int used : 1;
    }; /* la taille de notre en-tête est donc de 32 bits, soit un int */

    /* on veut qu'un bloc ait une taille minimale */
    #define MALLOC_MINSIZE 16

    void *malloc (size_t sz)
    {
        struct malloc_header_t *bl = NULL;

        /* taille totale des données à allouer */

        size_t realsize = sz + sizeof (struct malloc_header_t);

        /* pour des raisons de performances, on va aligner la taille à
         * allouer sur le multiple de 8 supérieur
         */
        size_t i = realsize % MALLOC_MINSIZE;

        if (i != 0)
        {
            realsize = realsize - i + MALLOC_MINSIZE;
        }

        /* on initialise d'abord le tas si cela n'a pas déjà été fait */
        if (HeapStart == 0)
        {
            HeapStart = sbrk (realsize);

            /* en cas d'erreur, sbrk() renvoie -1 */
            if (HeapStart == (char *) -1)
            {
                return NULL;
            }

            /* on défini la fin du tas */
            HeapEnd = realsize;

            /* on déclare notre premier bloc */
            bl       = (struct malloc_header_t *) HeapStart;
            bl->size = realsize;
            bl->used = 0; /* l'allocation ne se fait pas tout de suite */
        }
        else
        {
            /* on cherche le premier bloc de données libre de taille supérieure ou égale */
            bl = (struct malloc_header_t *) HeapStart;

            while (bl->used || bl->size < realsize)
            {
                /* on passe au bloc suivant */
                bl = (struct malloc_header_t *) (bl + bl->size);

                /* si on a atteint la fin du tas, alors on en redemande */
                if (bl == (struct malloc_header_t *) HeapEnd)
                {
                    HeapEnd = sbrk (realsize);

                    if (HeapEnd == (char *) -1)
                    {
                        return NULL;
                    }

                    /* on crée notre nouveau bloc */
                    bl       = (struct malloc_header_t *) HeapEnd;
                    bl->size = realsize;
                    bl->used = 0; /* l'allocation ne se fait pas tout de suite */

                    /* on redéfinit la fin du tas */
                    HeapEnd += realsize;
                }
            }
        }

        /* un bloc de données de taille supérieure ou égale a été trouvé */

        /* si la différence entre la taille du bloc que l'on a trouvé et
         * la taille des données que l'on veut est supérieure à la taille
         * minimale d'un bloc de données, alors on découpe le bloc pour
         * économiser la mémoire du tas.
         */
        if (bl->size - realsize > MALLOC_MINSIZE)
        {
            struct malloc_header_t *newbl = NULL;

            /* grâce à l'alignement fait au début, nos blocs ont tous une taille
             * en multiple de 8.
             */
            newbl       = (struct malloc_header_t *) ((char *) bl + realsize);
            newbl->size = bl->size - realsize;
            newbl->used = 0;

            /* on redéfini notre bloc actuel */
            bl->size = realsize;
        }

        /* on peut enfin définir notre bloc comme étant alloué */
        bl->used = 1;

        /* et on retourne un pointeur sur la mémoire qui suit notre en-tête */
        return (char *) (bl + sizeof (struct malloc_header_t));
    };

### Désallouer de la mémoire

Pour la déallocation, il suffira de définir le bloc actuel comme étant libre.
Et si le bloc suivant est également libre, on ajoute sa taille au bloc actuel,
afin d'optimiser la gestion du tas :

    :::c
    void free(char *addr)
    {
        struct malloc_header_t *bl = NULL;
        struct malloc_header_t *nextbl = NULL;

        /* On récupère notre en-tête */
        bl = (struct malloc_header_t *) (addr - sizeof (struct malloc_header_t));

        /* On merge le bloc nouvellement libéré avec le bloc suivant ci celui-ci
         * est aussi libre
         */
        while ((nextbl = (struct malloc_header_t *) ((char *) bl + bl->size))
                && nextbl < (struct malloc_header_t *) HeapEnd
                && nextbl->used == 0)
        {
            bl->size += nextbl->size;
        }

        /* On libère le bloc alloué */
        bl->used = 0;
    }

### Réallouer de la mémoire

Maintenant que nous avons les bases de notre gestionnaire, il serait bien de le
compléter avec des fonctions telles que ``realloc()`` et ``calloc()``.

Rappelons le fonctionnement de ``realloc()`` :

    :::c
    void *realloc (void *addr, size_t new_sz);

* Si ``addr`` est égal à ``NULL``, le fonctionnement est le même que ``malloc()``.
* Si ``new_sz`` est égal à ``0``, le fonctionnement est le même que ``free()``.
* Dans le cas contraire, on alloue le nouvel espace, on copie la mémoire de l'un vers l'autre, et on libère le précédent.

Voici ce que cela donne :

    :::c
    void *realloc (void *addr, size_t new_sz)
    {
        void *new_ptr = NULL;

        if (addr == NULL)
        {
            return malloc (new_sz);
        }

        if (new_sz == 0)
        {
            free (addr);
            return NULL;
        }

        /* on alloue le nouvel espace mémoire */
        new_ptr = malloc (new_sz);

        if (new_ptr != NULL)
        {
            struct malloc_header_t *bl = NULL;

            /* on récupère la taille de notre précédent pointeur */
            bl = (struct malloc_header_t *) (addr - sizeof (struct malloc_header_t));

            /* on recopie la mémoire */
            memcpy (new_ptr, addr, bl->size - sizeof (struct malloc_header_t));
        }

        return new_ptr;
    }

### Et pour finir

Il ne manque plus qu'une fonction à notre gestionnaire, celle ci est plutôt simple :

    :::c
    void *calloc (size_t nmemb, size_t membsz)
    {
        void *ptr = malloc (nmemb * membsz);

        if (ptr != NULL)
        {
            memset (ptr, 0, nmemb * membsz);
        }

        return ptr;
    }

## Conclusion

L'implémentation d'un gestionnaire de mémoire n'est donc pas compliqué. On comprend
maintenant mieux le fonctionnement des fuites de mémoire, et les gros risques qu'elle
peut entraîner.

En effet, le tas et la pile partageant le même registre, si l'on ne libère pas la
mémoire, on diminue la taille de la pile, et nous risquons de faire planter gravement
le programme (empiler des données risquerait d'écrire par dessus de la mémoire allouée
dynamiquement).
