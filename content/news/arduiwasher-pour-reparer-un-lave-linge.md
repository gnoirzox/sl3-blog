Title: Présentation du projet ArduiWasher
Date: 2013-01-24 12:00:00
Author: Étienne Dubois
Tags: arduino, etienne51, c, cpp, electronique

Pour bien débuter, en tant que membre du SL3, je vais vous présenter un projet sur lequel je travaille actuellement, ArduiWasher. Il s'agit de mon premier projet concret utilisant le microcontrôleur d'Arduino en tant que contrôleur et programme d'un lave-linge.
Pour ceux qui se demandent ce qu'est l'[Arduino](http://owni.fr/2011/12/16/arduino-naissance-mythe-bidouille/), l'idée est de permettre à n'importe qui de réaliser des montages électroniques plus ou moins élaborés, d'une façon assez simple, en ayant recours à la programmation (langage C/C++).

Le programme d'origine du lave-linge (programme mécanique, du type carte à trous) ne fonctionnait plus correctement, il m'est alors venu l'idée de tenter d'en réaliser un électronique, ce qui éviterait de tout remplacer.
Je me suis donc penché sur les divers actionneurs et détecteurs intégrés, comprendre le fonctionnement de chacun d'entre eux (sauf le système de chauffage, actuellement non utilisé, je le verrais un peu plus tard).

Actionneurs:

- Moteur (rotations du tambour)
- Solénoïde (arrivée d'eau)
- Pompe (vidange)
- Interlock (verrouillage)

Détecteurs:

- Pression (indique la fin du remplissage)
- Interlock (indique que la porte est fermée et vérouillée)
- Divers boutons de choix du programme et de lancement

Il m'a fallu considérer séparément chaque élément un par un pour comprendre son fonctionnement, le moteur m'a pris assez longtemps, quelques recherches sur Internet, diverses mesures, et divers essais. Le solénoïde et la pompe aucun souci. J'ai démonté l'interlock pour comprendre comment il fonctionnait, de ce fait j'ai pu le cibler facilement aussi. Pour le capteur de pression j'ai simulé un remplissage en y branchant ma pompe de dessouder pour faire office de source de pression, et j'ai testé les entrées-sorties voir comment réagissait le composant.
Pour chaque élément j'ai réalisé un schéma de ciblage simplifié pour l'interfacer avec l'Arduino.

Voilà par exemple le schéma de contrôle moteur.

![selenoide](https://dl.dropbox.com/u/1479070/DIY/Electronics/Washing%20Machine/selni_control.png)

Et ici, le schéma du système de remplissage/vidange d'eau.

![](https://dl.dropbox.com/u/1479070/DIY/Electronics/Washing%20Machine/filling_leak_control.png)

J'ai également réalisé les schémas des divers modules nécessaires à la carte contrôleur.



![](https://dl.dropbox.com/u/1479070/DIY/Electronics/Washing%20Machine/relay_control.png)

Détecteur de courant alternatif (220VAC). Celui-ci m'a pris du temps, beaucoup de recherche sur internet, et d'essai en conditions réelles.

![detector](https://dl.dropbox.com/u/1479070/DIY/Electronics/Washing%20Machine/ac_detector.png)

Les composants électroniques nécessaires à la réalisation de ces circuits venaient en partie de mon kit de débutant de l'Arduino, et surtout en grande partie d'un ancien onduleur qui trainait dans la remise. Je l'ai désassemblé pour récupérer des diodes, les relais, et des condensateurs de sécurité utiles aux circuits de détection AC.

Une fois tout les schémas et circuits terminés et en état de fonctionnement, je me suis mis au code source du programme sur l'Arduino ! Donc pour présenter les choses déjà, je n'utilise pas l'IDE Arduino. Etant habitué à développer en général, je trouve l'environnement par défaut trop minimal, aucune auto-complétion, impossibilité de programmer en orienté objet et j'en passe... Je me suis mis à la recherche d'une solution pour développer réellement en C/C++ et avec un environnement plus approprié. J'ai pu découvrir que l'IDE Arduino n'est en réalité qu'une interface pour tous les outils en ligne de commande (gcc, g++,...). Il existait divers [tutoriels](http://playground.arduino.cc/code/eclipse) permettant de configurer un environnement Eclipse pour développer correctement. J'ai donc pu configurer ainsi mon IDE favori (j'utilisais Eclipse depuis un bon moment déjà pour développer en Java sous Android) pour faire du développement pour la plateforme Arduino.

Certaines fonctionnalités du langage C++ ne sont pas utilisables en développant sur cette plateforme, comme les collections, de plus, il n'est pas conseillé d'utiliser abusivement les opérateurs new et delete, par exemple. Je m'étais fait piéger un jour par un int que j'utilisais comme timer, qui dépassait la valeur maximale possible, et qui résultait en un bug que j'ai eu du mal à trouver pendant un moment). [Ne pas oublier donc, sur un Arduino Uno, un int correspond à un entier de 16-bits](http://arduino.cc/en/Reference/Int).

Pour revenir au programme, j'ai opté pour la séparation de la gestion de chaque élément par classes :

- actuators/Interlock.h (verrouillage)
- actuators/Motor.h (moteur)
- actuators/Water.h (arrivée d'eau, et vidange)

Le dossier 'programs' contient l'ensemble des classes correspondant à chaque programme de lavage. Actuellement un seul programme est disponible à des fins de test, le programme 'Default'. La gestion sera améliorée une fois que tout les bugs seront corrigés, et il sera possible via un affichage, de choisir le programme que l'on souhaite lancer, et d'avoir un timer indiquant le temps restant avant la fin du lavage. Les possibilités sont quasi-infinies, ensuite, avec une peu d'imagination. Il est possible par exemple de relier le tout au réseau, afin de régler à distance un timer, sélectionner le programme depuis son smartphone, et pourquoi pas, si une erreur survient, l'envoyer à un serveur syslog ?

Le projet se trouve sur GitHub à l'adresse suivante: [https://github.com/etienne51/ArduiWasher](https://github.com/etienne51/ArduiWasher)


<a href="https://dl.dropbox.com/u/1479070/DIY/Electronics/Washing%20Machine/img3.jpg"><img src="https://dl.dropbox.com/u/1479070/DIY/Electronics/Washing%20Machine/img3.jpg" alt="arduiwasher" width="400" /></a>



Etienne Dubois.
