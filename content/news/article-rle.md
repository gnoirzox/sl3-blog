Title: La compression de données avec RLE
Date: 2013-01-28 18:00:00
Author: David Delassus
Tags: compression, python

La compression de données est un domaine assez important dans le monde de
l'informatique. C'est notre unique moyen pour transférer des données à la taille
conséquente de manière plus rapide. Et il existe de nombreux algorithmes que l'on
classe dans deux catégories : compression avec pertes, compression sans pertes.

Dans la première catégorie, on a par exemple l'algorithme **JPEG**. Dans la
seconde catégorie, on aura l'algorithme **RLE**, dont nous étudierons le
fonctionnement dans la suite de cet article.

## La compression avec pertes

Le principe de ce type de compression est de réduire la taille des données en
supprimant les données jugées superflues.

Prenons l'exemple de l'aglorithme **JPEG**. Ce dernier va repérer les pixels de
l'image qui n'apporte à celle ci que de la qualité (du détail) et les supprimer.

Le problème de ce genre de compression est qu'il est impossible de récupérer ses
données lors de la décompression.

## La compression sans pertes

Contrairement à la compression avec pertes, la compression sans pertes va
préserver les données. Pour cela, on encode les données pour en réduire la taille.


Autant dire que c'est ce qui nous intéresse, on a comme algorithmes :

* RLE, que nous étudierons ici ;
* tar ;
* RAR ;
* LZMA ;
* GZip, BZip, ZIP ;
* CPIO ;
* etc.

## Un exemple avec RLE

**RLE** signifie _**R**un-**L**ength **E**ncoding_. Le principe est simple, pour
chaque bloc de données identique, on l'écrit précédé du nombre de fois qu'il
apparaît consécutivement. Pour la décompression, il suffit de lire le nombre
d'occurences, et d'écrire l'entité autant de fois que nous le dicte le nombre.

Prenons cete chaîne de caractères :

    WWWWWWBBBBBTTTTTGGGGDDDDEEEEWWWW

Une fois l'algorithme _RLE_ effectué, les données compressées ressembleront à
ceci :

    6W5B5T4G4D4E4W

Vous voyez tout de suite que les données sont plus courtes ! Faisons un peu de
calcul. Notre chaîne initiale a une longueur de **32** caractères, la chaîne
compressée a une longueur de **14** caractères, on cherche ``X`` le taux de
compression :

    X = 14 * 100 / 32
    X = 43.75

Nous avons un taux de compression de **43,75%**. C'est pas mal ! Cependant,
faisons la même chose pour cette chaîne :

    WBWBWBWBWBWBWBWB
    -->
    1W1B1W1B1W1B1W1B1W1B1W1B1W1B1W1B

La taille a doublé ! Calculons à nouveau le taux de compression :

    X = 32 * 100 / 16
    X = 200

Notre taux de compression est de **200%** !

_Note: Il est facile de s'apercevoir que plus le taux de compression est faible,
plus l'algorithme est performant._

Bien que l'algorithme _RLE_ soit très peu performant, il est notamment utilisé
pour le format d'image **BMP**. Il est quand même intéressant de l'aborder pour
une introduction.

## Implémentation en langage Python

Implémentons cet algorithme avec le langage Python :

    :::python
    # -*- coding: utf-8 -*-

    # Ce module nous permettra de regrouper notre chaîne de caractères
    # en une liste de tuples ayant pour structure :
    # ( n occurences, élément )

    import itertools

    def encode(string):
        encoded = []

        # itertools.groupby() va grouper notre chaîne en une
        # liste de tuples contenant chacun l'occurence et la
        # liste de chacune d'entre elles
        for name, group in itertools.groupby(string):
            # on va donc créer un nouveau tuple dont le premier
            # élément est la longueur de la liste, soit le nombre
            # d'occurences, et le deuxième membre, l'occurence en
            # elle même
            block = (
                len(list(group)),
                name
            )

            # on ajoute ensuite ce tuple à la liste
            encoded.append(block)

        return encoded

        # ou alors, en une seule ligne :
        # return [(len(list(group)), name) for name , group in itertools.groupby(string)]

    def decode(rle):
        decoded = []

        # pour chaques tuples ( N , DATA ) de nos données compressées
        for block in rle:
            # on ajoute N fois DATA à notre liste
            decoded += block[0] * [block[1]]

        return decoded

        # ou alors, en une seule ligne :
        # return sum([b[0] * [b[1]] for b in rle], [])

## Conclusion

L'algorithme **RLE** est simpliste, et permet de voir toutes les problématiques
liées à la compression de données :

* le taux de compression dépend du type de données à compresser ;
* l'implémentation de l'algorithme n'est pas le plus complexe (comme toujours) ;
* le choix du type d'algorithme à utiliser selon le type de données n'est pas
  toujours évident.

Dans un prochain article, je vous parlerai de **Huffman**.
